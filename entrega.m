clc; clear; close all; 
% ************************************************************************
%                           ECG PRE-PROCESING
% ************************************************************************

% LOAD DATA
load('ecgConditioningExample.mat');
old_ecg = ecg;

% CONNECTION TEST
test_connection(ecg);

% ZERO ALIGN
ecg = alignment(ecg);

% RECORDING SPIKES REMOVAL
ecg = rmspikes(ecg, 25, 75, 5);

% POWERLINE INTERFERENCE FILTERING
ecg = pwfilt(50, fs, 1, ecg);    

% LOW-PASS FILTERING
ecg = lpfilt(90, fs, ecg);

% BASELINE WANDER REMOVAL
ecg = rmbaseline(fs, ecg); 

plotting(ecg, 1, 2000, 5000);

function [data] = rmbaseline(fs, data)
    for i = 1:length(data(1,:))
        if ~isnan(data(:,i))
            % Nyquist
            fn = fs/2;   
            % Normalised pass and stop band
            Wp = [1  100]/fn;                                       
            Ws = [0.5  110]/fn;                                    
            Rp = 1;                                                 
            Rs = 3;
            % Filter process
            [n,Ws] = cheb2ord(Wp, Ws, Rp, Rs);                     
            [b,a] = cheby2(n, Rs, Ws);                              
            [sos,g] = tf2sos(b,a);                               
            data(:,i) = filtfilt(sos,g,data(:,i));
        end
    end
end   

function [filtered] = lpfilt(fc, fs, noisy)
    % Butter low pass filter
    [b,a] = butter(20,fc/(fs/2));
    for i = 1:length(noisy(1,:))
        filtered(:,i) = filter(b, a, noisy(:,i));
    end
end

function [filtered] = pwfilt(pwhz, fs, q, noisy)
    % Notch at 50 Hz
    wo = pwhz/(fs/2);  
    bw = wo/q;
    [b,a] = iirnotch(wo,bw);
    for i = 1:length(noisy(1,:))
        filtered(:,i) = filter(b, a, noisy(:,i));
    end
end

function [inlier] = rmspikes(data, lq, uq, factor)
    for i = 1:length(data(1,:))
        % Interquartil Range calculation
        IQR = iqr(data(:,i)); 
        low = prctile(data(:,i),lq)-factor*IQR;
        high = prctile(data(:,i),uq)+factor*IQR; 
        % Outlier detection and updating for mean value
        outliers = find(data(:,i)<low | data(:,i)>high);
        for j = 1:length(outliers)
           data(outliers(j),i) = mean(data(:,i)); 
        end
        inlier = data;
    end
end
function [aligned] = alignment(not_aligned)
    % Moving data to zero
    for i = 1:length(not_aligned(1,:))
        aligned(:,i) = (not_aligned(:,i) - mean(not_aligned(:,i)))/std(not_aligned(:,i));
    end
    
end
function [] = test_connection(ecg)
    for i = 1:length(ecg(1,:))
        % Check if there are values in the channel
        val = sum(ecg(:,i) == 0);
        if val == length(ecg(:,1))
            fig = uifigure;
            nstr = num2str(i);
            str = strcat('Canal nº', nstr, ' ha estado desconectado durante todo el registro.');
            uialert(fig, str, '¡Atención!'); 
        end
    end
end

% Plotting function for debugging and results visualization 
function [] = plotting(data, p, xlim1, xlim2)
    if p == 1
        figure;
        subplot(2,3,1), plot(data(:,1));
        xlim([xlim1 xlim2]);
        subplot(2,3,2), plot(data(:,2));
        xlim([xlim1 xlim2]);
        subplot(2,3,3), plot(data(:,3));
        xlim([xlim1 xlim2]);
        subplot(2,3,4), plot(data(:,4));
        xlim([xlim1 xlim2]);
        subplot(2,3,5), plot(data(:,5));
        xlim([xlim1 xlim2]);
        subplot(2,3,6), plot(data(:,6));
        xlim([xlim1 xlim2]);
    end
end
